-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 24, 2021 at 08:51 AM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.4.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `codeigniter_hmvc`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `getUserByEmail` (IN `mail` CHAR(100))  BEGIN
SELECT id,code,name,email,mobile,password,created_at FROM `users`
WHERE email = mail
LIMIT 1;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getUserByPhone` (IN `mob` CHAR(15))  BEGIN
SELECT id,code,name,email,mobile,password,created_at FROM `users`
WHERE mobile = mob
LIMIT 1;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertUserData` (IN `code` CHAR(15), IN `name` CHAR(50), IN `email` CHAR(100), IN `mobile` CHAR(15), IN `password` VARCHAR(255))  BEGIN
INSERT INTO `users` (`code`, `name`, `email`, `mobile`, `password`) VALUES (code, name, email, mobile, password);
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `password_reset`
--

CREATE TABLE `password_reset` (
  `email` char(100) NOT NULL COMMENT 'email',
  `reset_token` varchar(255) NOT NULL COMMENT 'token',
  `request_at` datetime NOT NULL COMMENT 'time'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `password_reset_log`
--

CREATE TABLE `password_reset_log` (
  `email` char(100) NOT NULL COMMENT 'password reset email',
  `reset_done_at` datetime NOT NULL COMMENT 'reset done timestamp'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `profile`
--

CREATE TABLE `profile` (
  `profile_id` int(11) NOT NULL COMMENT 'AI,PK',
  `user_id` int(11) DEFAULT NULL COMMENT 'user id',
  `profile_dob` date DEFAULT NULL COMMENT 'user date of birth',
  `profile_picture` char(10) DEFAULT NULL COMMENT 'user image'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `profile`
--

INSERT INTO `profile` (`profile_id`, `user_id`, `profile_dob`, `profile_picture`) VALUES
(4, 4, NULL, NULL),
(5, 5, NULL, NULL),
(6, 6, NULL, NULL),
(7, 7, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL COMMENT 'AI,PK',
  `code` char(15) DEFAULT NULL COMMENT 'user code',
  `name` char(50) NOT NULL COMMENT 'name',
  `email` char(100) NOT NULL COMMENT 'email address',
  `mobile` char(15) NOT NULL COMMENT 'mobile number',
  `password` varchar(255) NOT NULL COMMENT 'password',
  `created_at` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'created at',
  `status` int(1) NOT NULL DEFAULT 0 COMMENT 'status : 1=>active,0=>inactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `code`, `name`, `email`, `mobile`, `password`, `created_at`, `status`) VALUES
(4, 'HMVC-YmWiQKyqnH', 'DEBAPRASAD MAITY', 'debaprasad.maity@yahoo.com', '1234567891', '202cb962ac59075b964b07152d234b70', '2021-01-09 19:47:06', 0),
(5, 'HMVC-6SafDdp57O', 'DEBAPRASAD MAITY', 'dmparision@gmail.com', '7647583746', '202cb962ac59075b964b07152d234b70', '2021-01-09 19:54:17', 0),
(6, 'HMVC-GjTtL37mJ5', 'Kamal Barman', 'kamal@gmail.com', '7856432512', '202cb962ac59075b964b07152d234b70', '2021-01-09 19:55:53', 0),
(7, 'HMVC-ea4PbVKxGj', 'David Johnson Maity', 'david_johnson@mailinator.com', '9831843237', '202cb962ac59075b964b07152d234b70', '2021-01-24 12:03:32', 0);

--
-- Triggers `users`
--
DELIMITER $$
CREATE TRIGGER `afterUserInsert` AFTER INSERT ON `users` FOR EACH ROW INSERT INTO profile(user_id) VALUES(new.id)
$$
DELIMITER ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `profile`
--
ALTER TABLE `profile`
  ADD PRIMARY KEY (`profile_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `profile`
--
ALTER TABLE `profile`
  MODIFY `profile_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'AI,PK', AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'AI,PK', AUTO_INCREMENT=8;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `profile`
--
ALTER TABLE `profile`
  ADD CONSTRAINT `profile_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
