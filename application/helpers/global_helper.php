<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
*------------------------------
* check user authentication
*-----------------------------
*/
if ( ! function_exists('check_auth'))
{
        function check_auth()
        {
			$CI =& get_instance();
            //echo "<pre>";print_r($CI->session->userdata('userdata'));exit;
            if(!$CI->session->userdata('userdata')['loginStatus']){
            	redirect(base_url().'login.html');
            }
            return true;
        }
}
/**
*--------------------------------------------------------
* is loged in user then redirect to user dashboard
*--------------------------------------------------------
*/
if( ! function_exists('is_loggedIn') )
{
	function is_loggedIn()
	{
		$CI =& get_instance();
        //echo "here";exit;
		if(!$CI->session->userdata('userdata')){
			return false;        	
        }
        else{
        	return true;
        }
	}
}