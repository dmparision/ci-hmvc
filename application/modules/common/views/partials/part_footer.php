<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright © <?= date('Y-m-d H:i:s')?> <a href="">Debaprasad Maity</a>.</strong> All rights
    reserved.
  </footer>