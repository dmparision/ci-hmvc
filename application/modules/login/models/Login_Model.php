<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Login_Model extends CI_Model {

	function __construct()
	{
		Parent::__construct();
	}
	/**
	*-----------------------------------------------------
	* checkCredentials user by email and do the loged in
	*-----------------------------------------------------
	*/
	public function checkCredentials()
	{
		//echo '<pre>';print_r($_POST);
		$result = $this->db->query("CALL getUserByEmail(?)",$this->input->post('username'))->result();
		if( is_array($result) && count($result)== 1) {
			//echo '<pre>';print_r($result);
			if($result[0]->password == $this->input->post('password')){
				//password match
				return [
					'loginStatus' =>true,
					'email' => $result[0]->email,
					'name' => $result[0]->name,
					'code' => $result[0]->code,
					'mobile' => $result[0]->mobile,
					'created_at' => $result[0]->created_at,
				];

			}else{
				//password don't match
				return [
					'loginStatus' => false,
					'msg' => 'Invalid Creddentials'
				];

			}
		}else{
			//echo 'not found';
			return [
				'loginStatus' => false,
				'msg' => 'Invalid Creddentials'
			];			
		}		
	}
}
