<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//load captcha library
include_once APPPATH.'libraries\captcha\Captcha.php';

class Login extends MX_Controller {
	//properties ...
	public $data;
	public $vals;
	public $cap;
	public $loginAttempt;
	public $title='Admin Login';
	function __construct()
	{
		Parent::__construct();		
		$this->load->model('Login_Model');
		$this->load->helper('captcha');
		$this->load->helper('security');
		$this->load->library(array('Form_validation'));
		$this->loginAttempt = 0;
		//init captcha library
		$this->captcha =  new Captcha();
		$this->config->set_item('pageTitle',$this->title);
		$this->data['header'] = $this->load->view('common/prelogin/header');
		$this->data['footer'] = $this->load->view('common/prelogin/footer');
	}
	public function index()
	{		
		if(is_loggedIn()){
			redirect('dashboard');
		}else{
			$this->data['cap'] =$this->captcha->getCaptcha();
			$this->load->view('login_view',$this->data);
		}
		//$this->Login_Model->call();
	}
	/**
	*-----------------------------------------------------
	* checkCredentials user by email and do the loged in
	*-----------------------------------------------------
	*/
	public function doLogin()
	{		
		$this->setLoginAttempt();
		$this->form_validation->set_rules('username', 'Username', 'trim|required|valid_email|max_length[100]');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[3]|md5');
		$this->form_validation->set_rules('captcha', 'Captcha', 'required|callback_check_captcha');

		if ($this->form_validation->run($this) == FALSE){
            
            $this->data['cap'] =$this->captcha->getCaptcha();
	
			$this->load->view('login_view',$this->data);
        }
        else{
        	//validation is ok        	
        	$result = $this->Login_Model->checkCredentials();        	
        	if($result['loginStatus']){

        		/*
        		$this->session->set_flashdata('success','Valid Credentials');
				redirect(base_url().'login');
				*/

				//set session
				$this->session->set_userdata('userdata',$result);
				redirect('dashboard');
        	}else{
        		//set login attempt
        		
        		$this->session->set_flashdata('error',$result['msg']);
				redirect('login');
        	}
        }
	}	
	/**
	*----------------------------------------------
	* callback function
	*----------------------------------------------
	*/
	public function check_captcha($string)
	{
		//echo $this->session->userdata('captcha_answer').'='.$string;exit;
	   	if(empty($string)):

			$this->form_validation->set_message('check_captcha', 'CAPTCHA CODE is required');

	      	return false;
	   	elseif($string != $this->session->userdata('captcha_answer')):

	      $this->form_validation->set_message('check_captcha', 'Please Enter VALID CAPTCHA CODE!');

	      return false;
	   	else:

	      return true; 
	   	endif;
	}
	/**
	*------------------------------------------------------
	* set login attempt in 60 sec upto 5 time
	*------------------------------------------------------
	*/
	private function setLoginAttempt()
	{
		//var_dump($this->session->userdata('loginAttempt'));
		//echo time();
		if( !$this->session->userdata('loginAttempt') ):
			//first time only
			$this->loginAttempt+=1;
			$this->session->set_userdata('loginAttempt',$this->loginAttempt);
			$this->session->set_userdata('loginTime',time());
		else:
			if($this->session->userdata('loginAttempt') < $this->config->item('login_attempt')):
				//allowed login attempt defined in config
				$this->loginAttempt = $this->session->userdata('loginAttempt');
				$this->loginAttempt++;
				$this->session->set_userdata('loginAttempt',$this->loginAttempt);
			else:				
				if( time() - $this->session->userdata('loginTime')  < $this->config->item('login_throttle')):
					//exceed allowed login attempt with in time
					echo '<script>alert("Max Login limit Reached  in 1 minute UpTo - 5");</script>';
				else:
					//reset login attempt
					$this->loginAttempt = 0;
					$this->session->set_userdata('loginAttempt',$this->loginAttempt);
				endif;
			endif;
		endif;		
	}
	/**
	*--------------------------------------------------
	* get username as email or mobile number
	*--------------------------------------------------
	*/
	public function getUsername()
	{
		/*
		echo "<pre>";print_r($_POST);
		$username = (filter_var($this->input->post('username'), FILTER_VALIDATE_EMAIL))?'email':'phone';
       
        echo $username;
		exit;
		*/
		try{

		}catch(Exception $e){

		}
	}
	/**
	*--------------------------------------
	* Logout user and destroy usersession
	*---------------------------------------
	*/
	public function doLogOut()
	{
		$this->session->sess_destroy();
		//$this->session->unset_userdata('userdata');
		redirect('login','refresh');		
	}
}

