<?php
  echo $header;
?>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
     <a ><b>Codeigniter</b>&nbsp;HMVC</a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Sign in to start your session</p>

      <?php
        $attributes = array('class' => 'login', 'id' => 'login',);
        echo form_open('doLogin', $attributes);
      ?>
      <div class="form-group has-feedback <?php echo !empty(form_error('username'))?'has-error':''; ?>">
        <input type="text" class="form-control" placeholder="Email" id= "username" name="username" value="<?php echo set_value('username'); ?>">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
         <span class="help-block"><?php echo form_error('username'); ?></span>
      </div>
      <div class="form-group has-feedback <?php echo !empty(form_error('password'))?'has-error':''; ?>">
        <input type="password" class="form-control" id="password" placeholder="Password" name="password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
         <span class="help-block"><?php echo form_error('password'); ?></span>
      </div>

      <!--CAPTCHA-->
      <div class="row">
        <div class="form-group col-xs-9">
          <?php echo $cap['image'];?>
        </div>
        <div class="form-group col-xs-3"> 
          <button type="button" id="reloadcaptcha" onClick=" window.location.reload();">Refresh</button>
        </div>        
      </div>
      <!--CAPTCHA--> 
      <div class="form-group has-feedback  <?php echo !empty(form_error('captcha'))?'has-error':''; ?>">
        <input type="text" class="form-control" autocomplete="off" id="captcha"  name="captcha" placeholder="Enter text shown in image">
        
        <span class="help-block"><?php echo form_error('captcha'); ?></span>
      </div> 
      <div class="row">
        <div class="col-xs-12">
           <button type="submit" class="btn btn-success btn-block btn-flat">Login</button>
           <?php 
            if(!empty($this->session->userdata('loginAttempt'))):
              //echo "<span class='text-danger'>You have ".($this->config->item('login_attempt')- $this->session->userdata('loginAttempt'))." attempt left in 60 Sec</span>";

            endif;
          ?>
        </div>

      </div>    
      <!-- <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox"> Remember Me
            </label>
          </div>
        </div> -->
        <!-- /.col -->        
      <?
        echo form_close();
      ?>

    <div class="social-auth-links text-center">
      <!-- <p>- OR -</p>
      <a href="javascript:void(0);" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in using
        Facebook</a>
      <a href="javascript:void(0);" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign in using
        Google+</a> -->
    </div>
    <!-- /.social-auth-links -->
    <?php
      echo anchor('password', "I forgot my password<br/>", 'title="Click To Go Password Reset Page"');
      echo anchor('signup', 'Register a new membership', 'title="Click To Go Registration Page"');

    ?>
    <!-- 
    <a href="<?php echo base_url()?>password">I forgot my password</a><br>
    <a href="<?php echo base_url()?>signup" class="text-center">Register a new membership</a>
    -->
  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<script>
  
  var msg = "<?php if($this->session->userdata('success')) echo $this->session->userdata('success');else echo $this->session->userdata('error');?>";
  var loginAttempt = "<?php echo $this->session->userdata('loginAttempt'); ?>";
  console.log(loginAttempt);
  //form validation
  function checkLoginCredentials()
  {
    
    if($("#username").val() == ""){
      swal("Please Enter Email", "", "error");
      $("#username").focus();
      return false;
    }else if($("#password").val() == ""){
      swal("Please Enter Passsword", "", "error");
      $("#password").focus();
      return false;
    }else if($("#captcha").val() == ""){
      swal("Please Enter Captcha Code", "", "error");
      $("#captcha").focus();
      return false;
    }else{
      return true;
    }
    
    //return true;
  }
</script>
  <?php
   if(!empty($this->session->userdata('success'))){
     echo '<script>swal(msg, "Thanks", "success");</script>';
   }else if(!empty($this->session->userdata('error'))){
    echo '<script>swal(msg, "OOPs..", "error");</script>';
   }    
    
  ?>
<?php
  echo $footer;
?>
