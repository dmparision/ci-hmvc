<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class User_Model extends CI_Model {

	function __construct(){
		$this->load->helper('string');
	}

	/**
	*---------------------------------------------
	* save function insert the data
	* into database during resgistration
	*---------------------------------------------
	*/
	public function saveUserData()
	{
		/*
			echo 'saveUserData is now calling';		
		*/		
        $data = array(
        	'code' => 'HMVC-'.random_string('alnum',10),
        	'name' => $this->input->post('name'),
        	'email' => $this->input->post('email'),
        	'mobile' => $this->input->post('mobile'),
        	'password' => $this->input->post('password')
        );
        return $this->db->query("CALL insertUserData(?,?,?,?,?)", $data)?TRUE:FALSE;
        //echo $this->db->last_query();
        //var_dump($result);exit;
	}
	/**
	*--------------------------------------------------
	* update function update user the data
	* into database after resgistration
	*--------------------------------------------------
	*/
	public function updateUserData($data = [],$id="")
	{
		echo 'updateUserData is now calling';
	}
	/**
	*-------------------------------------------------------------
	* check user by email and do the activate of user account
	*-------------------------------------------------------------
	*/
	public function activateUser($email="")
	{
		echo $email;
	}
	


}
