<?php
  echo $header;
?>
<body class="hold-transition login-page">
<div class="register-box">
  <div class="register-logo">
    <a ><b>Codeigniter</b>&nbsp;HMVC</a>
  </div>
    
  <div class="register-box-body">
    <p class="login-box-msg">Register a new membership</p>

    <!-- <form action="" method="post"> -->
      <?php
        $attributes = array('class' => 'registration', 'id' => 'registration');
        echo form_open('signup/userRegistration', $attributes);
      ?>
      <div class="form-group has-feedback <?php echo !empty(form_error('name'))?'has-error':''; ?>">
        <input type="text" class="form-control" autocomplete="off" name="name" value="<?php echo set_value('name'); ?>" placeholder="Full name">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
        <span class="help-block"><?php echo form_error('name'); ?></span>
      </div>
      <div class="form-group has-feedback  <?php echo !empty(form_error('email'))?'has-error':''; ?>">
        <input type="text" class="form-control"name="email"  value="<?php echo set_value('email'); ?>" autocomplete="off"  placeholder="Email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        <span class="help-block"><?php echo form_error('email'); ?></span>
      </div>
      <div class="form-group has-feedback  <?php echo !empty(form_error('mobile'))?'has-error':''; ?>">
        <input type="text" class="form-control" autocomplete="off" name="mobile"  value="<?php echo set_value('mobile'); ?>"   placeholder="Mobile">
        <span class="glyphicon glyphicon-phone form-control-feedback"></span>
        <span class="help-block"><?php echo form_error('mobile'); ?></span>
      </div>
      <div class="form-group has-feedback  <?php echo !empty(form_error('password'))?'has-error':''; ?>">
        <input type="password" class="form-control" autocomplete="off"  name="password" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        <span class="help-block"><?php echo form_error('password'); ?></span>
      </div>
      <div class="form-group has-feedback  <?php echo !empty(form_error('cofirm_password'))?'has-error':''; ?>">
        <input type="password" class="form-control" autocomplete="off"  name="cofirm_password" placeholder="Confirm password">
        <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
        <span class="help-block"><?php echo form_error('cofirm_password'); ?></span>
      </div>      
      <!--CAPTCHA-->
      <div class="row">
        <div class="form-group col-xs-9">
          <?php echo $cap['image'];?>
        </div>
        <div class="form-group col-xs-3"> 
          <button type="button" id="reloadcaptcha" onClick=" window.location.reload();">Refresh</button>
        </div>        
      </div>
      <!--CAPTCHA--> 
      <div class="form-group has-feedback  <?php echo !empty(form_error('captcha'))?'has-error':''; ?>">
        <input type="text" class="form-control" autocomplete="off"  name="captcha" placeholder="Enter text shown in image">
        
        <span class="help-block"><?php echo form_error('captcha'); ?></span>
      </div> 
      <div class="row">
        <div class="col-xs-12">
           <button type="submit" class="btn btn-success btn-block btn-flat">Register</button>
        </div>
      </div>    
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <!-- <label>
              <div class="icheckbox_square-blue" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="checkbox" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div> I agree to the <a href="#">terms</a>
            </label> -->
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <!-- <button type="submit" class="btn btn-success btn-block btn-flat">Register</button> -->
        </div>
        <!-- /.col -->
      </div>
    <!-- </form> -->
    <?php
      echo form_close();
    ?>

    <div class="social-auth-links text-center">
      <!-- <p>- OR -</p>
      <a href="javascript:void(0);" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign up using
        Facebook</a>
      <a href="javascript:void(0);" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign up using
        Google+</a> -->
    </div>
    <?php
      echo anchor('doLogin','I already have a membership','title="Click to Go Login Page . . . "');
    ?>
    </div>
  <!-- /.form-box -->
</div>
<!-- /.login-box -->

<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });
  });
  var msg = "<?php if($this->session->userdata('success')) echo $this->session->userdata('success');else echo $this->session->userdata('error');?>";
</script>
  <?php
   if(!empty($this->session->userdata('success'))){
     echo '<script>swal(msg, "Thanks", "success");</script>';
   }else if(!empty($this->session->userdata('error'))){
    echo '<script>swal(msg, "OOPs..", "error");</script>';
   }

     
      //echo '<script>alert(msg);</script>';
    ?>

<?php
  echo $footer;
?>