<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH.'libraries\captcha\Captcha.php';

class Signup extends MX_Controller {
	//properties ...
	public $data;
	public $vals;
	public $cap;
	public $title='Registration Page';
	function __construct()
	{
		Parent::__construct();
		$this->config->set_item('pageTitle',$this->title);
		$this->load->model('User_Model');
		$this->load->helper('captcha');
		$this->load->helper('security');
		$this->load->library(array('Form_validation'));

		$this->captcha =  new Captcha();
		$this->data['header'] = $this->load->view('common/prelogin/header');
		$this->data['footer'] = $this->load->view('common/prelogin/footer');
	}

	/**
	*------------------------------
	* load signup view
	*------------------------------
	*/
	public function index()
	{				
		$this->data['cap'] = $this->captcha->getCaptcha();
	
		$this->load->view('signup_view',$this->data);
	}
	
	/**
	*---------------------------------------
	* submit registration data
	*--------------------------------------
	*/
	public function userRegistration()
	{
		//validation
		//echo "<pre>";print_r($_POST);exit;
		$this->form_validation->set_rules('name', 'Name', 'trim|required|min_length[10]|max_length[20]');

		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|max_length[100]|is_unique[users.email]');

		$this->form_validation->set_rules('mobile', 'Mobile', 'trim|required|min_length[10]|max_length[15]|is_unique[users.mobile]');

		$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[3]|md5');

		$this->form_validation->set_rules('cofirm_password', 'Confirm Password', 'trim|required|matches[password]|md5');

		$this->form_validation->set_rules('captcha', 'Captcha', 'callback_check_captcha');

				
		if ($this->form_validation->run($this) == FALSE){
            //echo "<pre>";print_r(validation_errors());exit;
            
            $this->data['cap'] = $this->captcha->getCaptcha();
	
			$this->load->view('signup_view',$this->data);
        }else{
            
            //echo "Form is processing";
            //echo "<pre>";print_r($_POST);exit;
            if( $this->User_Model->saveUserData($_POST) ){
            	//send an email notification to activate user account
            	/*
				$to = $this->input->post('email');
				$subject = "HTML email";
				$link = anchor("signup/acivateAccount/activate"."?email=".$_GET['email'],
		 		"<button type='button' style='color:white,background:#4CAF50;color: white;background: #4CAF50;border: 1px solid;height: 50px;width: 30%;hover: cursor;cursor: pointer;font-size: 20px;'>
					Click here to activate your account
				</button>",
				'title="Click here"');
				$message = "
				<html>
					<head>
					<title>HTML email</title>
					</head>
					<body>
						<h3 style='color:#24a1b1'>
							Thank you for your EXCELLENCY  Dear .".$this->input->post('name').".
						</h3>
						.".$link.".
					   
					</body>
					</html>
				";
				// Always set content-type when sending HTML email
				$headers = "MIME-Version: 1.0" . "\r\n";
				$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

				// More headers
				$headers .= 'From: <webmaster@example.com>' . "\r\n";
				//$headers .= 'Cc: myboss@example.com' . "\r\n";
				$res = mail($to,$subject,$message,$headers);
				if($res){
					$this->session->set_flashdata('success',"Enrolment Done Successfull,Please Check Email to ACTIVATE ACCOUNT ! ");
					redirect(base_url().'signup');					
				}else{

            		$this->session->set_flashdata('error',"Enrolment Done Successfull,UNABLE TO SEND EMAIL PLEASE CONTACT WITH US ! ");
					redirect(base_url().'signup');
				}
				*/
				$this->session->set_flashdata('success',"Enrolment Done Successfull,Please login . . .");
					redirect(base_url().'signup');
            }else{
            	$this->session->set_flashdata('error',"Enrolment Not Done,PLEASE TRY AGAIN LATER ! ");
					redirect(base_url().'signup');
            }                        
        }
	}

	
	//callback function
	public function check_captcha($string)
	{
		if(empty($string)):

			$this->form_validation->set_message('check_captcha', 'CAPTCHA CODE is required');
	      	return false;
	   	elseif($string != $this->session->userdata('captcha_answer')):

	      $this->form_validation->set_message('check_captcha', 'Please Enter VALID CAPTCHA CODE!');
	      return false;
	   else:

	      return true; 
	   endif;
	}

}
