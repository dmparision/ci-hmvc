<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Kolkata');
class Dashboard extends MX_Controller {
	//properties ...
	public $data;
	public $title='Admin Dashboard';
	function __construct()
	{
		Parent::__construct();
		check_auth();		
		$this->config->set_item('pageTitle',$this->title);
		$this->data['header'] = $this->load->view('common/postlogin/header','',TRUE);
		//$this->data['sidebar'] = $this->load->view('common/postlogin/sidebar','',TRUE);
		$this->data['footer'] = $this->load->view('common/postlogin/footer','',TRUE);
	}

	public function index()
	{
		//print_r($_SESSION);
		$this->load->view('dashboard/dashboard',$this->data);
	}

}		