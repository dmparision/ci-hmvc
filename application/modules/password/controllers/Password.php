<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH.'libraries\captcha\Captcha.php';
class Password extends MX_Controller {

	public $data;
	function __construct()
	{
		Parent::__construct();	
		$this->load->helper('captcha');	
		$this->load->library(array('Form_validation'));

		$this->captcha =  new Captcha();
	}
	public function index()
	{
		
        $this->data['cap'] = $this->captcha->getCaptcha();

		$this->load->view('password_reset',$this->data);
	}
	/**
	*-----------------------------
	* password reset request
	*------------------------------
	*/
	public function sendPasswordResetLink()
	{
		// echo "<script>alert('sendPasswordResetLink is WORKING');</script>";
		// $this->session->set_flashdata('work','RESET PASSWORD LINK IS WORK IN PROGESS !');		
		// echo "<script>window.location.href='".base_url().'password'."';</script>";
		//return redirect(base_url().'password');

		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|max_length[100]');
		$this->form_validation->set_rules('captcha', 'Captcha', 'required|callback_check_captcha');


		if ($this->form_validation->run($this) == FALSE){
            
            $this->data['cap'] =$this->captcha->getCaptcha();
	
			$this->load->view('password_reset',$this->data);
        }else{
        	// do proceed

        	//check email exit or not

        	//send email
			echo "<script>alert('sendPasswordResetLink is WORKING');</script>";
			$this->session->set_flashdata('work','RESET PASSWORD LINK IS WORK IN PROGESS !');		
			echo "<script>window.location.href='".base_url().'password'."';</script>";
        }

	}
	
	/**
	*----------------------------------------------
	* callback function
	*----------------------------------------------
	*/
	public function check_captcha($string)
	{
		
		//echo $this->session->userdata('captcha_answer').'='.$string;exit;
		if(empty($string)):

			$this->form_validation->set_message('check_captcha', 'CAPTCHA CODE is required');
	      	return false;
	   	elseif($string != $this->session->userdata('captcha_answer')):

	      $this->form_validation->set_message('check_captcha', 'Please Enter VALID CAPTCHA CODE!');
	      return false;
	   else:

	      return true; 
	   endif;
	}
}
