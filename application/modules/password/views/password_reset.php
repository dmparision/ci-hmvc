
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Sign up</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url()?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url()?>assets/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url()?>assets/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url()?>assets/dist/css/AdminLTE.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url()?>assets/plugins/iCheck/square/blue.css">

  
  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
<div class="register-box">
  <div class="register-logo">
     <a ><b>Codeigniter</b>&nbsp;HMVC</a>
  </div>

  <div class="register-box-body">
    <p class="login-box-msg">Password Reset Form</p>

    <?php
      $attr = ['id'=>'sendPasswordResetLink'];
      echo form_open('password/sendPasswordResetLink',$attr);
    ?>      
      <div class="form-group has-feedback <?php echo !empty(form_error('email'))?'has-error':''; ?>">
        <input type="email" class="form-control" name="email" value="<?php echo set_value('email');?>" placeholder="Email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        <span class="help-block"><?php echo form_error('email'); ?></span>
      </div>     
      <!--CAPTCHA-->
      <div class="row">
        <div class="form-group col-xs-9">
          <?php echo $cap['image'];?>
        </div>
        <div class="form-group col-xs-3"> 
          <button type="button" id="reloadcaptcha" onClick=" window.location.reload();">Refresh</button>
        </div>        
      </div>
      <!--CAPTCHA--> 
      <div class="form-group has-feedback  <?php echo !empty(form_error('captcha'))?'has-error':''; ?>">
        <input type="text" class="form-control" autocomplete="off"  name="captcha" placeholder="Enter text shown in image">
        
        <span class="help-block"><?php echo form_error('captcha'); ?></span>
      </div>  
      <div class="row">        
        <!-- /.col -->
        
        <div class="col-xs-4">
          <?php
            echo anchor('', '<button type="button" class="btn btn-success btn-block btn-flat">Login</button>', 'title="Click To Go LoginPage"');
          ?>
          <!-- <a href="<?php echo base_url()?>" class="text-center">
            <button type="button" class="btn btn-success btn-block btn-flat">Login</button>
          </a> -->
        </div>
        <div class="col-xs-8">
          <button type="submit" class="btn btn-danger btn-block btn-flat">Send Password Reset Link</button>
        </div>
        <!-- /.col -->
      </div>
    <?php

      echo form_close();
    ?>    
  </div>
  <!-- /.form-box -->
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="<?php echo base_url()?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url()?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?php echo base_url()?>assets/plugins/iCheck/icheck.min.js"></script>
<!--sweet alert-->
<script src="<?php echo base_url()?>assets/plugins/sweetAlert/sweetalert.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });
  });
var msg = "<?php if($this->session->userdata('work')) echo $this->session->userdata('work');else echo "";?>";
</script>
  <?php
   if(!empty($this->session->userdata('work'))){
     echo '<script>swal(msg, "Thanks", "success");</script>';
   }     
  ?>
</body>
</html>
