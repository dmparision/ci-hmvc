<?php
/**
*
* Captcha class is used to create captcha code
* it will call from any class.
* to use this class first need to required this library
* then create object in class constructor
* then call getCaptcha()
*
*/
class Captcha{

	//prperties
	private $imgURL = '';
	private $CI = '';
	private $vals = '';
	private $operator1='';
	private $operator2='';
	private $fontPath='';
	function __construct(){
        $this->CI = & get_instance();
    }

	/**
	*------------------------------------------------
	* generate capctcha code
	*-------------------------------------------------
	*/
	public function getCaptcha($length=7)
	{
		//$this->generateMathProblem();
		//create captcha array
		$this->vals = array(
	        'word'          => $this->generateMathProblem(),
	        'img_path'      => './captcha/',
	        'img_url'       => 'http://localhost/HMVC/captcha/',
	        'font_path'     => $this->fontPath,	        
	        'img_width'     => '250',
	        'img_height'    => 50,
	        'expiration'    => 900,
	        'word_length'   => 6,
	        'font_size'     => 15,
	        'img_id'        => 'Imageid',
	        'pool'          => '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',

	        // White background and border, black text and red grid
	        'colors' => array(
                'background' => $this->getBackgoundColor(),
                'border' => $this->getTextColor(),
                'text' => $this->getTextColor(),
                'grid' => $this->getTextColor(),
        	)
		);
		//echo '<pre>';print_r($this->vals);exit;
		$this->cap = create_captcha($this->vals);		
		$this->CI->session->set_userdata('captcha_answer',$this->cap['word']);
		return $this->cap;
	}
	
	/**
	*----------------------------------
	* get token as math or text
	*----------------------------------
	*/
	private function getCaptchaType()
	{
		/*
		$array = array('text','add','sub','mul'); 		
		shuffle($array);
		return array_rand($array);
		*/
		return rand(0,3);
		//echo '<pre>';print_r(array_rand($type));die();
	}
	/**
	*-----------------------------------------------
	* generate random math problem
	*-----------------------------------------------
	*/
	private function generateMathProblem()
	{
		$word = null;
		switch($this->getCaptchaType()){
			case 0:
				$word = $this->token($length=7);
				$this->fontPath = FCPATH.'captcha/fonts/7.ttf';				
			break;
			case 1:
				$word = $this->getAddition();
				$this->fontPath = './path/to/fonts/texb.ttf';				
			break;
			case 2:
				$word = $this->getSubtraction();
				$this->fontPath = './path/to/fonts/texb.ttf';				
			break;
			case 3:
				$word = $this->getMultiplication();	
				$this->fontPath = './path/to/fonts/texb.ttf';			
			break;
			default:
				$word = $this->token($length=7);
				$this->fontPath = FCPATH.'captcha/fonts/4.ttf';
		}
		return $word;
		exit;
	}
	/**
	*--------------------------------------------------------------
	* getAddition return a addition problem as a string 
	*-------------------------------------------------------------
	*/
	private function getAddition()
	{
		return  rand(1,30).'+'.rand(2,30).'+'.rand(1,9).'=';
	}
	/**
	*----------------------------------------------------------
	* getSubtraction return a subtraction problem as a string 
	*----------------------------------------------------------
	*/
	private function getSubtraction()
	{
		return  rand(20,50).'-'.rand(5,20).'=';
	}
	/**
	*---------------------------------------------------------------
	* getMultiplication return a multiplication problem as a string 
	*-----------------------------------------------------------------
	*/
	private function getMultiplication()
	{
		return  rand(1,9).'*'.rand(2,7).'*'.rand(1,9).'=';
	}
	/**
	*--------------------------------------
	* get random token
	*-------------------------------------
	*/
	private function token($length=""){
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}
	/**
	*----------------------------------------------------
	* get random text color
	*----------------------------------------------------
	*/
	private function getTextColor()
	{
		return [rand(10,25),rand(1,255),rand(1,255)];
	}
	/**
	*--------------------------------------------
	* get random background color
	*-------------------------------------------------
	*/
	private function getBackgoundColor()
	{
		return [rand(200,255),rand(200,255),rand(250,255)];
	}

}

